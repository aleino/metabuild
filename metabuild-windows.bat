@echo off

REM === arguments ===
set vc_varsall_path=%1
set external_source_directory_path=%2
set source_file_path=%3
set target_file_path=%4
set object_code_file_path=%~dpn4.obj
set target_directory_file_path=%~dp4
set target_type=%5

REM === target parameters ===
set target_architecture=x64

REM === disabled warnings ======
REM I like to do while(1) loops
set disable_conditional_expression_is_constant_warning_flag=/wd4127
REM Sometimes I have to include headers that define inline functions I wont call
set disable_unreferenced_inline_function_warning_flag=/wd4514
REM Sometimes, functions are indeed not inlined
set disable_function_not_inlined_warning_flag=/wd4710
REM Sometimes, funtions are indeed inlined
set disable_function_inlined_warning_flag=/wd4711
REM I cannot assign to float infinity if this is on
set disable_overflow_in_constant_arithmetic_warning_flag=/wd4756
REM I don't want warnings about padding being inserted in structs
set disable_struct_padding_warning_flag=/wd4820
REM I don't mind spectre mitigations
set disable_spectre_mitigation_warning_flag=/wd5045
REM Potentially unsafe things are sometimes necessary
set disable_potentially_unsafe_methods_warning_flag=/wd4996
set disabled_warning_flags=^
    %disable_potentially_unsafe_methods_warning_flag%^
    %disable_overflow_in_constant_arithmetic_warning_flag%^
    %disable_conditional_expression_is_constant_warning_flag%^
    %disable_unreferenced_inline_function_warning_flag%^
    %disable_struct_padding_warning_flag%^
    %disable_function_not_inlined_warning_flag%^
    %disable_function_inlined_warning_flag%^
    %disable_spectre_mitigation_warning_flag%

REM === compiler flags ===
set generate_intrinsic_functions_flag=/Oi
set disable_optimizations_flag=/Od
set full_speed_optimization_flag=/Ox
set generate_7_0_compatible_debug_info_flag=/Z7
set supress_startup_logo_flag=/nologo
set warning_are_errors_flag=/WX
set use_multithreaded_release_dynamically_linked_runtime_library_flag=/MD
set use_multithreaded_debug_dynamically_linked_runtime_library_flag=/MDd
set disable_minimal_rebuilds_flag=/Gm-
set disable_all_exceptions_flag=/EHa-
set enable_all_warnings_flag=/Wall
set executable_output_flag=/Fe%target_file_path%
set object_output_flag=/Fo%object_code_file_path%
if %target_type% == debug (
   set target_type_specific_flags=^
       %set disable_optimizations_flag%^
       %generate_7_0_compatible_debug_info_flag%^
       %use_multithreaded_debug_dynamically_linked_runtime_library_flag%
)
if %target_type% == release (
   set target_type_specific_flags=^
       %full_speed_optimization_flag%^
       %use_multithreaded_release_dynamically_linked_runtime_library_flag%
)
set include_directory_flags=^
    /I%external_source_directory_path%
set compiler_flags=^
    %supress_startup_logo_flag%^
    %warning_are_errors_flag%^
    %enable_all_warnings_flag%^
    %disabled_warning_flags%^
    %generate_intrinsic_functions_flag%^
    %target_type_specific_flags%^
    %disable_minimal_rebuilds_flag%^
    %disable_all_exceptions_flag%^
    %include_directory_flags%^
    %object_output_flag%^
    %executable_output_flag%
    
REM === linker flags ===
set disable_incremental_linking_flag=/incremental:no
set linker_flags=^
    %disable_incremental_linking_flag%

REM === compile command ===
set compile_command=^
    cl^
    %compiler_flags%^
    %source_file_path%^
    /link %linker_flags%

REM make the directory unless it already exists
if not exist %target_directory_file_path% mkdir %target_directory_file_path%

call %vc_varsall_path% %target_architecture%
call %compile_command%
