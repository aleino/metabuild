(setq metabuild-script-name
      (cond ((eq system-type 'windows-nt) "metabuild-windows.bat")
            ('t "metabuild-unix.sh")))

(defun metabuild-arguments (vc-install-path external-source-directory-path source-file-path target-file-path type)
  (interactive)
  (concat vc-install-path " "
          external-source-directory-path " "
          source-file-path " "
          target-file-path " "
          (cond ((eq type 'release) "release")
                ((eq type 'debug) "debug"))))

(defun metabuild-build-command (vc-install-path external-source-directory-path source-file-path target-file-path type)
  (interactive)
  (concat metabuild-path
          metabuild-script-name " "
          (metabuild-arguments vc-install-path
                               external-source-directory-path
                               source-file-path
                               target-file-path
                               type)))
